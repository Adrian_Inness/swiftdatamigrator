# Moves SWIFT data from Production to Staging using a JSON extraction file to get filenames.
import json
import urllib.request
import configparser
import mimetypes

config = configparser.RawConfigParser()
config.read('SWIFTConfig.properties')

# ----------------------------------------------------------------------------------------
# Returns a security token from the token server.
# @param    environment The environment section to use in the SWIFTConfig.properties file
# @return   token       The security token from the token server
def getToken(environment) :
    data = json.dumps({"auth": {"identity": {"methods": ["password"],
                                             "password": {"user": {"name": config.get(environment, 'OS_USERNAME'),
                                                                   "domain": {"id": config.get(environment, 'OS_PROJECT_DOMAIN_ID')},
                                                                   "password": config.get(environment, 'OS_PASSWORD')}}}}}).encode("utf-8")
    tokenRequest = urllib.request.Request(config.get(environment, 'OS_AUTH_URL'), data,
                                          {'Content-Type': 'application/json'})
    with urllib.request.urlopen(tokenRequest) as tokenResponse:
        token = tokenResponse.headers['X-Subject-Token']
    return token
# ----------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------
# Returns a file retrieved from the SWIFT storage server
# @param    token       The security token from the token server
# @param    fileName    The name of the file to retrieve.
# @return   file        Binary file contents
def getFile(token, fileName) :
    headers = {
        "X-Auth-Token": token,
        "Accept": "application/json"
    }
    swiftRequest = urllib.request.Request(fileName, headers=headers)
    swiftRequest.get_method = lambda: 'GET'
    with urllib.request.urlopen(swiftRequest) as swiftResponse:
        return swiftResponse.read()
# ----------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------
# Uploads a file to the SWIFT storage server
# @param    token       The security token from the token server
# @param    fileName    The name of the file to retrieve.
# @param    file        Binary file contents
def putFile(token, fileName, file) :
    headers = {
        "X-Auth-Token": token,
        "Accept": "application/json",
        "Content-Type": mimetypes.guess_type(fileName)[0]+";",
        "Content-Length": len(file)
    }
    swiftRequest = urllib.request.Request(fileName, file, headers=headers)
    swiftRequest.get_method = lambda: 'PUT'
    with urllib.request.urlopen(swiftRequest) as swiftResponse:
        return swiftResponse.read()
# ----------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------
# Read Configuration
EXTRACTIONS = config.get("GENERAL", 'EXTRACTIONS').replace(" ", "").split(',')
x = 1
for jsonFile in EXTRACTIONS:
    with open(jsonFile) as f:
        body = json.load(f)
        total = len(body)
        t = 1
        for element in body:
            print (jsonFile+" ported "+str(t)+" of "+str(total))
            t = t + 1
            FIELDS = config.get("GENERAL", 'FIELDS_'+str(x)).replace(" ", "").split(',')
            for field in FIELDS:
                try:
                    fileUrl = element[field]
                except KeyError:
                    next
        x = x + 1
